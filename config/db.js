import mysql from 'mysql';
import dotenv from 'dotenv';
import Sequelize from 'sequelize';

dotenv.load();
const {
  RDS_HOSTNAME,
  RDS_USERNAME,
  RDS_PASSWORD,
  RDS_PORT,
  RDS_DB_NAME,
} = process.env;

export default mysql.createPool({
  host: RDS_HOSTNAME,
  user: RDS_USERNAME,
  password: RDS_PASSWORD,
  database: RDS_DB_NAME,
  port: RDS_PORT,
});

export const sequelize = new Sequelize(RDS_DB_NAME, RDS_USERNAME, RDS_PASSWORD, {
  host: RDS_HOSTNAME,
  port: RDS_PORT,
  dialect: 'mysql',
  logging: false,
});
