import express from 'express';
import cors from 'cors';
import bodyParser from 'body-parser';
import morgan from 'morgan';
import dotenv from 'dotenv';
import accountRoutes from './app/routes/account';
import authRoutes from './app/routes/auth';
import authMiddleware from './app/middlewares/auth';

global.__basedir = __dirname;

dotenv.load();
const app = express();
const port = process.env.PORT || 5002;

var http = require('http').Server(app);

app.use(cors());
app.disable('etag');

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use(morgan('dev'));

app.use('/api/auth', authRoutes);
app.use('/api/account', authMiddleware, accountRoutes);


app.get('/health', (req, res) => {
  res.send('I am listening!');
});

http.listen(port);
