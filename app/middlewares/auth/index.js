import { Auth, User } from './../../models';
import { isTokenExpired } from './../../utils/auth';
import { findUserByAccessToken } from './../../services/sequelize/auth';
export const notAuthorized = (res) => res.status(401).json({
  error: 'not_authorized'
});

export default async (req, res, next) => {
  const { authorization } = req.headers;
  if (!authorization) {
    return notAuthorized(res);
  }

  const auth = await findUserByAccessToken(authorization).catch(() => notAuthorized(res));

  if (!auth || isTokenExpired(auth.createdAt, auth.expiresIn)) {
    return notAuthorized(res);
  }

  req.user = auth.user.dataValues;
  next();
}
