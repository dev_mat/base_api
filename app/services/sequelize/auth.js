import {
  User,
  Auth,
} from './../../models';
import { ACTIVE_USER } from './../../constants/user';

export const findUserByAccessToken = (accessToken) => Auth.findOne({
  where: { accessToken }, include: [
    {
      model: User,
      as: 'user',
      where: {
        status: ACTIVE_USER,
      }
    }
  ]
});
