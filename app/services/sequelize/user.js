import {
  User,
} from './../../models';
import { ACTIVE_USER } from './../../constants/user';

export const updateUser = (values, where) => User.update(values, { where });
export const findLoginUser = (email) => User.findOne({
  where: {
    status: ACTIVE_USER,
    $or: [
      {
        email: email
      },
      {
        username: email,
      },
    ]
  }
});
