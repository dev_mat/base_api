import { sequelize } from '../../../config/db';
import Sequelize from 'sequelize';
import { DEFAULT_TOKEN_EXPIRE_TIME } from './../../constants/auth';
export const model = {
  id: {
    type: Sequelize.INTEGER,
    primaryKey: true,
    autoIncrement: true,
    unique: true,
  },
  accessToken: {
    type: Sequelize.STRING,
    unique: true
  },
  refreshToken: {
    type: Sequelize.STRING,
    unique: true,
  },
  expiresIn: {
    type: Sequelize.INTEGER,
    defaultValue: DEFAULT_TOKEN_EXPIRE_TIME,
  },
};

export const tableName = 'auth';

const Auth = sequelize.define(tableName, model, {
  timestamps: true,
  charset: 'utf8',
  collate: 'utf8_unicode_ci'
});

export default Auth;


