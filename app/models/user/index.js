import { sequelize } from '../../../config/db';
import Sequelize from 'sequelize';

export const model = {
  id: {
    type: Sequelize.INTEGER,
    primaryKey: true,
    autoIncrement: true,
    unique: true,
  },
  name: {
    type: Sequelize.STRING,
  },
  username: {
    type: Sequelize.STRING,
    unique: true,
  },
  email: {
    type: Sequelize.STRING,
    unique: true,
  },
  password: {
    type: Sequelize.STRING,
  },
  city: {
    type: Sequelize.STRING,
  },
  gender: {
    type: Sequelize.ENUM(
      'MALE',
      'FEMALE'
    )
  },
  type: {
    type: Sequelize.ENUM(
      'ADMIN',
      'USER'
    )
  },
  status: {
    type: Sequelize.ENUM(
      'ACTIVE',
      'BLOCKED'
    )
  },
};

const User = sequelize.define('user', model, {
  timestamps: true,
  charset: 'utf8',
  collate: 'utf8_unicode_ci',
});

export default User;


