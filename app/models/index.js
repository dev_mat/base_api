import Auth from './auth/';
import User from './user/';

/* Authorization */
Auth.belongsTo(User, { sourceKey: 'user', as: 'user' });


export { default as Auth } from './auth';
export { default as User } from './user';
