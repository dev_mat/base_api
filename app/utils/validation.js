export const validateEmail = (email) => {
  var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(String(email).toLowerCase());
};

export const validateUsername = (username) => {
  var re = /^[a-zA-Z0-9_]+$/;;
  return re.test(String(username).toLowerCase());
};

export const validatePassword = (pass) => {
  return pass.length >= 6;
};
