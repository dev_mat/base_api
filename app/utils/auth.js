export const isTokenExpired = (tokenCreatedAt, tokenExpiresIn) => {
  const now = new Date();
  const createdAt = new Date(tokenCreatedAt);
  const expiresTime = parseInt(createdAt / 1000, 10) + tokenExpiresIn;
  if (parseInt(now / 1000, 10) > expiresTime) {
    return true;
  }
  return false;
};
