import uuidv4 from 'uuid/v4';
import bcrypt from 'bcrypt';
import {
  Auth,
  User,
} from './../../models';
import { DEFAULT_TOKEN_EXPIRE_TIME } from './../../constants/auth';
import { ACTIVE_USER } from './../../constants/user';
import { findLoginUser } from './../../services/sequelize/user';

export default async (req, res) => {
  const { email, password } = req.body;
  const user = await findLoginUser(email);
  if (!user) {
    return res.status(500).json({ error: 'Wrong email' });
  }

  const isPassCorrect = await bcrypt.compare(password, user.password);
  if (!isPassCorrect) {
    return res.status(500).json({ error: 'Wrong password' });
  }

  const newModel = {
    accessToken: uuidv4(),
    refreshToken: uuidv4(),
    expiresIn: DEFAULT_TOKEN_EXPIRE_TIME,
    userId: user.id,
  };
  const auth = Auth.create(newModel);

  res.json(newModel);
};
