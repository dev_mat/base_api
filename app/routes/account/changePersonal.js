import {
  User,
} from './../../models';
import { updateUser } from './../../services/sequelize/user';

export default async (req, res) => {
  const userData = req.user;
  const { name, gender, username, city } = req.body;

  const user = updateUser({
    name,
    gender,
    username,
    city
  }, { id: userData.id }).catch(e => {
    res.status(500).json({
      message: 'error',
    });
  });

  res.json({
    message: 'success',
  });
};
