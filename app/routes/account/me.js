import {
  User,
} from './../../models';

export default async (req, res) => {
  const { name, gender, username, city } = req.user;
  res.json({
    name, gender, username, city
  });
};
