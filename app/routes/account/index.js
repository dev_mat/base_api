import express from 'express';
import me from './me';
import changePersonal from './changePersonal';

const router = express.Router();

router.get('/me', me);
router.patch('/change-personal', changePersonal);
export default router;
